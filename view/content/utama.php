<!DOCTYPE html>
<html>
    <head>
      <?php  include '../layouts/_path.php' ?>
    </head>
  <body class="fixed-left">
    <?php include '../layouts/_flip.php' ?>
    <?php include '../layouts/_modallogout.php' ?>

    <div id="wrapper">
      <?php include '../layouts/_rightsidemenu.php' ?>
      <?php include '../layouts/_topside.php' ?>
      <?php include '../layouts/_leftside.php' ?>
      <?php include '../layouts/_examplecontent.php' ?>
      <?php include '../layouts/_foot.php' ?>
    </div>

    <?php include '../layouts/_endpath.php' ?>
  </body>
</html>
